import requests
import getpass

from classes import *


def init(): #starting connection with robinhood API
    sesh  = requests.session()
    sesh.headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "en;q=1, fr;q=0.9, de;q=0.8, ja;q=0.7, nl;q=0.6, it;q=0.5",
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "X-Robinhood-API-Version": "1.0.0",
            "Connection": "keep-alive",
            "User-Agent": "Robinhood/823 (iPhone; iOS 7.1.2; Scale/2.00)",
            "Origin": "https://robinhood.com"
            }
    sesh = login(sesh)

    sesh.account = account_info(sesh)

    return sesh

def login(sesh):
    usrnm = input('Username: ')
    psswrd = getpass.getpass()
    twofa = input('2FA Code: ')
    payload = {
            'username': usrnm,
            'password': psswrd,
            'mfa_code': twofa,
            'scope': 'internal',
            'grant_type': 'password',
            'client_id': 'c82SH0WZOsabOXGP2sxqcj34FxkvfnWRZBKlBjFS',
            'expires_in': 86400
            }
    res = sesh.post(endpoints['login'], data=payload) #interacting with login endpoint
    res.raise_for_status() #error raise if login failed
    print('Log In Successful')

    #removing sensitive information
    payload['password'] = None
    payload['mfa_code'] = None

    sesh.login = res #saving login results
    sesh.payload = {**payload, ** res.json()} #merging two dictionaries
    sesh.payload['token'] = sesh.payload['refresh_token']
    sesh.headers['Authorization'] = 'Bearer ' + str(sesh.payload['access_token'])
    return sesh

def account_info(sesh):
    account_dat = sesh.get(endpoints['accounts'])
    return account_dat.json()['results'][0]

def logout(sesh):
    # sesh.payload['token'] = sesh.payload['refresh_token'] #requires token parameter, equivalent of refresh token
    res = sesh.post(endpoints['logout'], data = sesh.payload)
    # res = sesh.post(endpoints['logout'])
    res.raise_for_status() #error raise if logout failed
    print('Log Out Successful')
    return 0

# def stocks_quote(sesh, inputs):
#     # stocks = [x.upper() for x in stocks] #capitalize all
#     # if type(inputs.stocks) is not(list): #error check for list
#     #     raise TypeError('Not a list')
#
#     # sesh.headers['Authorization'] = 'Bearer ' + str(sesh.payload['access_token'])
#     url = str(endpoints['quotes']) + "?symbols=" + ",".join(inputs.stocks)
#     # dat = sesh.get(url, headers = sesh.headers) #headers defined in login function
#     dat = sesh.get(url)
#     dat.raise_for_status() #error if pulling stock quote failed
#     stocks_data = class_stocks(inputs.stocks, dat.json()['results'])
#
#     return stocks_data
#
# def stocks_historical(sesh, inputs):
#     # stocks = [x.upper() for x in stocks]
#     # interval = 'day' #(str)
#     # span = 'year' #(str)
#     # bounds = 'extended' #doesn't work
#
#     # url = str(endpoints['historicals'])+"/?symbols=" + ','.join(stocks) + "&interval=" + interval + "&span=" + span + "&bounds=" + bounds
#     url = str(endpoints['historicals'])+"/?symbols=" + ','.join(inputs.stocks) + "&interval=" + inputs.interval + "&span=" + inputs.span
#     dat = sesh.get(url)
#     dat.raise_for_status()
#
#     stocks_data = class_stocks(inputs.stocks, dat.json()['results'])
#
#     return stocks_data

def stocks_quote(sesh, inputs, type = 'quote'):
    #combined old stocks_quote and stocks_historical
    #'quote' = current time
    #'historical' = data for up to 5 years

    if type is 'quote':
        url = str(endpoints['quotes']) + "?symbols=" + ",".join(inputs.stocks)
    elif type is 'historical':
        url = str(endpoints['historicals'])+"/?symbols=" + ','.join(inputs.stocks) + "&interval=" + inputs.interval + "&span=" + inputs.span
    else:
        TypeError('unknown data type requested')

    dat = sesh.get(url)
    dat.raise_for_status()

    data = class_stocks(inputs.stocks, dat.json()['results'])

    return data

def stocks_order(sesh, inputs, side = 'buy'):
    inputs.payload['side'] = side
    inputs.payload['account'] = sesh.account['url']

    res = sesh.post(endpoints['orders'], data = inputs.payload)
    res.raise_for_status()
    print('Order to '+ side.upper() +' '+str(inputs.payload['quantity'])+ ' Share(s) of ' + inputs.payload['symbol'] + ' Successfully Placed')
    return 0
