import rh_func
from classes import *

#login
rh_sesh = rh_func.init()

stocks_list = ['PLUG', 'NVAX']

#quotes & historical
quote_inputs = rh_quote_inputs(stocks_list)
stocks_history = rh_func.stocks_quote(rh_sesh, quote_inputs, 'historical')
stocks_dat = rh_func.stocks_quote(rh_sesh, quote_inputs)

#placing order
order_inputs = rh_order_inputs(stocks_list[0], stocks_dat)
rh_func.stocks_order(rh_sesh, order_inputs, 'sell')

#logout
rh_func.logout(rh_sesh)
