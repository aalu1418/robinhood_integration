endpoints = {
    "login": "https://api.robinhood.com/oauth2/token/",
    "logout": "https://api.robinhood.com/oauth2/revoke_token/",
    "quotes":"https://api.robinhood.com/quotes/",
    "historicals":"https://api.robinhood.com/quotes/historicals/",
    "accounts": "https://api.robinhood.com/accounts/",
    "orders": "https://api.robinhood.com/orders/"
    }

class class_stocks:
    def __init__(self, list, dat):
        self.list = list
        self.dat = dat

class rh_quote_inputs:
    def __init__(self, stocks, interval = 'day', span = 'year'):

        # Note: valid interval/span configs
        #     interval = 5minute | 10minute + span = day | week
        #     interval = day + span = year
        #     interval = week + span = 5year

        if type(stocks) is not(list): #error check for list
            raise TypeError('not a list')

        self.stocks = [x.upper() for x in stocks] #capitalize all stock symbols
        self.interval = interval.lower()
        self.span = span.lower()

class rh_order_inputs:
    def __init__(self, stock_name, stock_dat, quantity = 1, tif = 'gfd', trigger = 'immediate', order_type = 'market'):
        stock_indice = stock_dat.list.index(stock_name)
        self.payload = {
            # 'account': self.get_account()['url'],
            'instrument': stock_dat.dat[stock_indice]['instrument'],
            'price': float(stock_dat.dat[stock_indice]['bid_price']),
            'quantity': quantity, #int
            # 'side': transaction.name.lower(),
            'symbol': stock_name, #str ALL CAPS
            'time_in_force': tif, #str 'gfd' or 'gtc'
            'trigger': trigger, #str 'immediate' or 'stop'
            'type': order_type #str 'market' or 'limit'
        }
