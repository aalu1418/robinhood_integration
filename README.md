# Robinhood API & Trading Bot Experiment/Project
The end goal is to have a bot that can trade penny stocks quickly and without supervision
while generating a profit.

#### Things to accomplish:
- [x] Login to Robinhood
- [x] Logout of Robinhood
- [x] Retrieve current stock price
- [x] Retrieve stock price history
- [x] Buy stock
- [x] Sell stock
- [ ] Retrieve information from some sort of list (top stocks, etc)
- [ ] AI integration (it's own mess to figure out)

#### Useful links:
*New Updates*
* https://medium.com/@tomgrek/heres-what-happened-when-i-gave-control-of-my-robinhood-account-to-an-ai-for-a-week-3309d62567c4
* http://www.robin-stocks.com/en/latest/#

*.md files*
* https://guides.github.com/features/mastering-markdown/

*requests python library*
* http://docs.python-requests.org/en/master/

*understanding curl commands*
* https://curl.haxx.se/docs/manpage.html
* https://curl.trillworks.com/#python

*robinhood api*
* https://github.com/sanko/Robinhood (old api stuff)
* https://github.com/anilshanbhag/RobinhoodShell
* https://github.com/Jamonek/Robinhood

*gathering stock data*
* https://github.com/mariostoev/finviz (stock screener)
* https://docs.python-guide.org/scenarios/scrape/ (html scraping - pulling data from websites)

*interesting things for future reference*
* http://gbeced.github.io/pyalgotrade/ (prediction algorithm)
* https://textblob.readthedocs.io/en/dev/ (Natural Language Processing)

#### Algorithm steps:
1. Find penny stocks performing well (using finviz api + html scraping)
2. Analyze stocks (using stock data + NLP + machine learning to tune prediction algorithm)
3. Buy best performing stocks according to prediction algorithm
